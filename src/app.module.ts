import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PersonalDetailModule } from './modules/personal-detail/personal-detail.module';
import { EducationModule } from './modules/education/education.module';
import { EmploymentHistoryModule } from './modules/employment-history/employment-history.module';
import { ExternalLinkModule } from './modules/external-link/external-link.module';
import { ProfessionalSummaryModule } from './modules/professional-summary/professional-summary.module';
import { SkillModule } from './modules/skill/skill.module';
import { ResumeModule } from './modules/resume/resume.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb+srv://backend:g2xeOkqqXunsEiTq@resume-saas-mongodb.1dgql.mongodb.net/resume-saas-1?retryWrites=true&w=majority',
    ),
    PersonalDetailModule,
    EducationModule,
    EmploymentHistoryModule,
    ExternalLinkModule,
    ProfessionalSummaryModule,
    SkillModule,
    ResumeModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
