import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type SkillDocument = Skill & Document

@Schema({
  toJSON: {
    transform(doc, ret){
      ret['id'] = ret._id
      delete ret._id
      delete ret.__v
    }
  }
})
export class Skill {
  @Prop()

  skillname: string
  @Prop()

  level: number
}

export const SkillSchema = SchemaFactory.createForClass(Skill)
