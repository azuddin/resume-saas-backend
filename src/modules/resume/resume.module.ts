import { Module } from '@nestjs/common';
import { ResumeService } from './resume.service';
import { ResumeController } from './resume.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonalDetail, PersonalDetailSchema } from '../personal-detail/entities/personal-detail.entity';
import { Education, EducationSchema } from '../education/entities/education.entity';
import { EmploymentHistory, EmploymentHistorySchema } from '../employment-history/entities/employment-history.entity';
import { ExternalLink, ExternalLinkSchema } from '../external-link/entities/external-link.entity';
import { ProfessionalSummary, ProfessionalSummarySchema } from '../professional-summary/entities/professional-summary.entity';
import { Skill, SkillSchema } from '../skill/entities/skill.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Education.name, schema: EducationSchema},
      { name: EmploymentHistory.name, schema: EmploymentHistorySchema},
      { name: ExternalLink.name, schema: ExternalLinkSchema},
      { name: PersonalDetail.name, schema: PersonalDetailSchema},
      { name: ProfessionalSummary.name, schema: ProfessionalSummarySchema},
      { name: Skill.name, schema: SkillSchema},
    ])
  ],
  controllers: [ResumeController],
  providers: [ResumeService]
})
export class ResumeModule {}
