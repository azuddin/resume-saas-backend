import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Education, EducationDocument } from '../education/entities/education.entity';
import { EmploymentHistory, EmploymentHistoryDocument } from '../employment-history/entities/employment-history.entity';
import { ExternalLink, ExternalLinkDocument } from '../external-link/entities/external-link.entity';
import { PersonalDetail, PersonalDetailDocument } from '../personal-detail/entities/personal-detail.entity';
import { ProfessionalSummary, ProfessionalSummaryDocument } from '../professional-summary/entities/professional-summary.entity';
import { Skill, SkillDocument } from '../skill/entities/skill.entity';
import { CreateResumeDto } from './dto/create-resume.dto';
import { UpdateResumeDto } from './dto/update-resume.dto';

@Injectable()
export class ResumeService {
  constructor(
    @InjectModel(Education.name) private EducationModel: Model<EducationDocument>,
    @InjectModel(EmploymentHistory.name) private EmploymentHistoryModel: Model<EmploymentHistoryDocument>,
    @InjectModel(ExternalLink.name) private ExternalLinkModel: Model<ExternalLinkDocument>,
    @InjectModel(PersonalDetail.name) private PersonalDetailModel: Model<PersonalDetailDocument>,
    @InjectModel(ProfessionalSummary.name) private ProfessionalSummaryModel: Model<ProfessionalSummaryDocument>,
    @InjectModel(Skill.name) private SkillModel: Model<SkillDocument>,
  ) {}

  create(createResumeDto: CreateResumeDto) {
    return 'This action adds a new resume';
  }

  findAll() {
    return 'This action adds a new resume';
  }

  async findOne(id: number) {
    const edu = await this.EducationModel.find().exec()
    const eh = await this.EmploymentHistoryModel.find().exec()
    const el = await this.ExternalLinkModel.find().exec()
    const ps = await this.ProfessionalSummaryModel.find().exec()
    const pd = await this.PersonalDetailModel.find().exec()
    const skill = await this.SkillModel.find().exec()

    return {
      education: edu,
      employment_history: eh,
      external_link: el,
      professional_summary: ps,
      personal_detail: pd,
      skill
    };
  }

  async update(id: number, updateResumeDto) {
    const data = JSON.parse(updateResumeDto.content);console.log(data)
    let edu
    let eh
    let el
    let ps
    let pd
    let skill

    //get data. if none, create new
    const existPD = await this.PersonalDetailModel.findOne().exec();console.log(existPD)
    if (!existPD) {
      pd = await new this.PersonalDetailModel({
        avatar: data.avatar,
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        phone: data.phone,
      }).save()
    } else {
      pd = await this.PersonalDetailModel.findByIdAndUpdate(existPD._id, {
        avatar: data.avatar,
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        phone: data.phone,
      }, {new: true});console.log(pd)
    }

    return {
      data: {
        education: edu,
        employment_history: eh,
        external_link: el,
        professional_summary: ps,
        personal_detail: pd,
        skill
      },
      message: 'Success'
    };
  }

  remove(id: number) {
    return `This action removes a #${id} resume`;
  }
}
