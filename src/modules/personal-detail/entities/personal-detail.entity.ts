import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type PersonalDetailDocument = PersonalDetail & Document

@Schema({
  toJSON: {
    transform(doc, ret){
      ret['id'] = ret._id
      delete ret._id
      delete ret.__v
    }
  }
})
export class PersonalDetail {
  @Prop()
  avatar: string
  
  @Prop()
  firstname: string
  
  @Prop()
  lastname: string
  
  @Prop()
  email: string
  
  @Prop()
  phone: string
}


export const PersonalDetailSchema = SchemaFactory.createForClass(PersonalDetail)
