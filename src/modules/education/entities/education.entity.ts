import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type EducationDocument = Education & Document

@Schema({
  toJSON: {
    transform(doc, ret){
      ret['id'] = ret._id
      delete ret._id
      delete ret.__v
    }
  }
})
export class Education {
  @Prop()
  degree: string
  
  @Prop()
  school: string
  
  @Prop()
  startdate: string
  
  @Prop()
  enddate: string
  
  @Prop()
  city: string
  
  @Prop()
  description: string
}

export const EducationSchema = SchemaFactory.createForClass(Education)
