import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ProfessionalSummaryService } from './professional-summary.service';
import { CreateProfessionalSummaryDto } from './dto/create-professional-summary.dto';
import { UpdateProfessionalSummaryDto } from './dto/update-professional-summary.dto';

@Controller('professional-summary')
export class ProfessionalSummaryController {
  constructor(private readonly professionalSummaryService: ProfessionalSummaryService) {}

  @Post()
  create(@Body() createProfessionalSummaryDto: CreateProfessionalSummaryDto) {
    return this.professionalSummaryService.create(createProfessionalSummaryDto);
  }

  @Get()
  findAll() {
    return this.professionalSummaryService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.professionalSummaryService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProfessionalSummaryDto: UpdateProfessionalSummaryDto) {
    return this.professionalSummaryService.update(+id, updateProfessionalSummaryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.professionalSummaryService.remove(+id);
  }
}
