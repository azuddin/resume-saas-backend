import { Injectable } from '@nestjs/common';
import { CreateProfessionalSummaryDto } from './dto/create-professional-summary.dto';
import { UpdateProfessionalSummaryDto } from './dto/update-professional-summary.dto';

@Injectable()
export class ProfessionalSummaryService {
  create(createProfessionalSummaryDto: CreateProfessionalSummaryDto) {
    return 'This action adds a new professionalSummary';
  }

  findAll() {
    return `This action returns all professionalSummary`;
  }

  findOne(id: number) {
    return `This action returns a #${id} professionalSummary`;
  }

  update(id: number, updateProfessionalSummaryDto: UpdateProfessionalSummaryDto) {
    return `This action updates a #${id} professionalSummary`;
  }

  remove(id: number) {
    return `This action removes a #${id} professionalSummary`;
  }
}
