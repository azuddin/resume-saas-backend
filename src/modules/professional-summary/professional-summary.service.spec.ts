import { Test, TestingModule } from '@nestjs/testing';
import { ProfessionalSummaryService } from './professional-summary.service';

describe('ProfessionalSummaryService', () => {
  let service: ProfessionalSummaryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProfessionalSummaryService],
    }).compile();

    service = module.get<ProfessionalSummaryService>(ProfessionalSummaryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
