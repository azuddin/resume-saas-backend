import { Module } from '@nestjs/common';
import { ProfessionalSummaryService } from './professional-summary.service';
import { ProfessionalSummaryController } from './professional-summary.controller';

@Module({
  controllers: [ProfessionalSummaryController],
  providers: [ProfessionalSummaryService]
})
export class ProfessionalSummaryModule {}
