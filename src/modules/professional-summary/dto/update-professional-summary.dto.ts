import { PartialType } from '@nestjs/mapped-types';
import { CreateProfessionalSummaryDto } from './create-professional-summary.dto';

export class UpdateProfessionalSummaryDto extends PartialType(CreateProfessionalSummaryDto) {}
