import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type ProfessionalSummaryDocument = ProfessionalSummary & Document

@Schema({
  toJSON: {
    transform(doc, ret){
      ret['id'] = ret._id
      delete ret._id
      delete ret.__v
    }
  }
})
export class ProfessionalSummary {
  @Prop()
  details: string
}

export const ProfessionalSummarySchema = SchemaFactory.createForClass(ProfessionalSummary)