import { Test, TestingModule } from '@nestjs/testing';
import { ProfessionalSummaryController } from './professional-summary.controller';
import { ProfessionalSummaryService } from './professional-summary.service';

describe('ProfessionalSummaryController', () => {
  let controller: ProfessionalSummaryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProfessionalSummaryController],
      providers: [ProfessionalSummaryService],
    }).compile();

    controller = module.get<ProfessionalSummaryController>(ProfessionalSummaryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
