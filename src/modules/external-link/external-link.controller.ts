import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ExternalLinkService } from './external-link.service';
import { CreateExternalLinkDto } from './dto/create-external-link.dto';
import { UpdateExternalLinkDto } from './dto/update-external-link.dto';

@Controller('external-link')
export class ExternalLinkController {
  constructor(private readonly externalLinkService: ExternalLinkService) {}

  @Post()
  create(@Body() createExternalLinkDto: CreateExternalLinkDto) {
    return this.externalLinkService.create(createExternalLinkDto);
  }

  @Get()
  findAll() {
    return this.externalLinkService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.externalLinkService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateExternalLinkDto: UpdateExternalLinkDto) {
    return this.externalLinkService.update(+id, updateExternalLinkDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.externalLinkService.remove(+id);
  }
}
