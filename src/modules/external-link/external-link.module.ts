import { Module } from '@nestjs/common';
import { ExternalLinkService } from './external-link.service';
import { ExternalLinkController } from './external-link.controller';

@Module({
  controllers: [ExternalLinkController],
  providers: [ExternalLinkService]
})
export class ExternalLinkModule {}
