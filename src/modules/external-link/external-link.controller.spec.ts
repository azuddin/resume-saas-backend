import { Test, TestingModule } from '@nestjs/testing';
import { ExternalLinkController } from './external-link.controller';
import { ExternalLinkService } from './external-link.service';

describe('ExternalLinkController', () => {
  let controller: ExternalLinkController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExternalLinkController],
      providers: [ExternalLinkService],
    }).compile();

    controller = module.get<ExternalLinkController>(ExternalLinkController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
