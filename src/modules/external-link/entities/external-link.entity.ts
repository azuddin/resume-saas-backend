import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type ExternalLinkDocument = ExternalLink & Document

@Schema({
  toJSON: {
    transform(doc, ret){
      ret['id'] = ret._id
      delete ret._id
      delete ret.__v
    }
  }
})
export class ExternalLink {
  @Prop()
  label: string
  
  @Prop()
  link: string
}

export const ExternalLinkSchema = SchemaFactory.createForClass(ExternalLink)
